import argparse
import csv
import datetime
import socket
import time
import functools
import operator
import itertools
from multiprocessing.managers import SyncManager
from multiprocessing import Queue, Process
import subprocess
import random
########################################################
# Author: Ronald Nieuwenhuis
# Assignment Big Data: Feel the pain!
# Due date: 18-05-'016
#######################################################



def calc_mean(occurrence_counter, total_phred_counter):
    """Calculate the average score per index."""
    average_scores = {}
    for key, value in occurrence_counter.items():
        average_scores[key] = round(total_phred_counter[key] / value)
    return average_scores


def merge_dicts(dict_list):
    """Merge the dictionaries with the averagescores."""
    all_keys = functools.reduce(operator.or_, (set(d.keys()) for d in dict_list), set())
    temp_dict = {k: [d[k] for d in dict_list if k in d] for k in all_keys}
    end_result = {key: round(sum(value) / len(value)) for key, value in temp_dict.items()}
    return end_result


def make_server_manager(port, authkey):
    """ Create a manager for the server, listening on the given port.
        Return a manager object with get_job_q and get_result_q methods.
    """
    job_q = Queue()
    result_q = Queue()

    # This is based on the examples in the official docs of multiprocessing.
    # get_{job|result}_q return synchronized proxies for the actual Queue
    # objects.
    class JobQueueManager(SyncManager):
        pass

    JobQueueManager.register('get_job_q', callable=lambda: job_q)
    JobQueueManager.register('get_result_q', callable=lambda: result_q)

    manager = JobQueueManager(address=('', port), authkey=authkey)
    manager.start()
    print('Server started at port %s' % port)
    return manager


def ssh_starter(host, server_ip, port, auth_key, numb_proc):
    """Start the clientscript on other pcs through ssh processes."""
    command = "ssh rnieuwenhuis@{}  python3.4 /commons/student/2016-2017/Thema12/rnieuwenhuis/clientscript.py -ip {} " \
              "-port {} -key {} -proc {}".format(host, server_ip, port, auth_key, numb_proc)
    try:
        subprocess.call(command.split(" "), stdin=None, stdout=None, stderr=None)
    except:
        raise NotImplementedError


def main():
    begin = datetime.datetime.now()
    h = open("/home/p225083/ronald/log.log", mode="a")
    parser = argparse.ArgumentParser(prog='script.py', description=__doc__, formatter_class=argparse.HelpFormatter)
    parser.add_argument('infile', type=str, nargs=1)
    parser.add_argument('-n', help='number of nodes used', required=False, default=[1], nargs=1, type=int)
    parser.add_argument('-p', help='number of processes on each node', required=False, default=[4], nargs=1, type=int)
    parser.add_argument('csvOut', nargs='?', type=str)
    args = parser.parse_args()
    h.write("started")
    # Get the managerserver IP adress. (The host name of the pc should probably work too. Because DNS.
    ip = [(s.connect(('8.8.8.8', 53)), s.getsockname()[0], s.close()) for s in
          [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]
    h.write(ip)
    # All of the host names available.
    host_names = ['bin103', 'bin134', 'bin145', 'bin201', 'bin214', 'bin256', 'bin302', 'bin010', 'bin140', 'bin207', 'bin251', 'bin308', 'bin104', 'bin135', 'bin146', 'bin202', 'bin215', 'bin257', 'bin303', 'bin130', 'bin141', 'bin210', 'bin252', 'bin309', 'bin105', 'bin136', 'bin150', 'bin203', 'bin216', 'bin258', 'bin304', 'bin100', 'bin131', 'bin142', 'bin211', 'bin253', 'bin310', 'bin106', 'bin137', 'bin151', 'bin204', 'bin230', 'bin305', 'bin101', 'bin132', 'bin143', 'bin212', 'bin254', 'bin300', 'bin311', 'bin107', 'bin138', 'bin152', 'bin205', 'bin231', 'bin306', 'bin102', 'bin133', 'bin144', 'bin200', 'bin213', 'bin255', 'bin301', 'bin330', 'bin108', 'bin139', 'bin153', 'bin206', 'bin250', 'bin307']
    #host_names = ['pg-node100', 'pg-node101', 'pg-node102', 'pg-node102']
    numb_nodes = args.n[0]
    numb_proc = args.p[0]

    manager = make_server_manager(5000, b"RN")
    shared_job_q = manager.get_job_q()
    shared_result_q = manager.get_result_q()

    # Pick [numb_proc] random machines to do the work.
    sample = random.sample(host_names, numb_proc)
    h.write(sample)
    #print(sample)
    procs = []
    for i in range(numb_nodes):
        p = Process(target=ssh_starter,
            args=(sample[i], ip, 5000, "RN", numb_proc)) # The port and aut_key are hardcoded.
        procs.append(p)
        p.start()

    with open(args.infile[0]) as fastq:
        iters = itertools.chain(fastq, (None,)*numb_nodes*numb_proc*4)
        for num_and_line in enumerate(iters, start=1):
            if num_and_line[0] % 4 == 0:            #This assumes a validated, non corrupted fastq file!!!!!
                    shared_job_q.put(num_and_line)

    # Join the processes.
    for p in procs:
        p.join()

    # produce data
    h.write(numb_nodes)
    results_list = []
    while len(results_list) < (numb_nodes*numb_proc):
        out_list = shared_result_q.get()
        results_list.append(out_list)
        h.write(len(results_list))

    time.sleep(2)
    manager.shutdown()

    timestamp_1 = datetime.datetime.now()
    average_list = []

    for occurrence_counter, total_phred_counter in results_list:
        h.write(occurrence_counter)
        h.write("---------------------------")
        average_scores = calc_mean(occurrence_counter, total_phred_counter)
        average_list.append(average_scores)
    end_result = merge_dicts(average_list)

    # Write output to csv file.
    timestamp_2 = datetime.datetime.now()
    if args.csvOut:
        with open(args.csvOut, mode="w") as csvFile:
            writer = csv.writer(csvFile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            for key, value in sorted(end_result.items()):
                writer.writerow([str(key), str(value)])
    else:
        for key, value in sorted(end_result.items()):
            print(key, value)

    end = datetime.datetime.now()
    print("Number of nodes: ", numb_nodes)
    print("Number of processes per node:", numb_proc)
    print("Total execution time: ", end - begin)
    print("Time used for iteration of fastq: ", timestamp_1 - begin)
    print("Time used for calculation of means and merging of dicts: ", timestamp_2 - timestamp_1)
    print("Time used for writing CSV: ", end - timestamp_2)
    print(numb_nodes, end - begin, timestamp_1 - begin, timestamp_2 - timestamp_1, end - timestamp_2)

if __name__ == "__main__":
    main()
