import sys
from multiprocessing.managers import SyncManager
from multiprocessing import Process
import argparse


########################################################
# Author: Ronald Nieuwenhuis
# Assignment Big Data: Feel the pain!
# Due date: 18-05-'016
#######################################################

def counter(in_queue, out_list):
    """Count both the occurrences of a base at a certain index and sum up their scores. """
    occurrence_counter = {}
    total_phred_counter = {}
    while True:
        item = in_queue.get(timeout=10)
        line_no, line = item
        if line != None:
            for i in range(1, len(line)):
                if not i in total_phred_counter and not i in occurrence_counter:
                    occurrence_counter[i] = 1
                    total_phred_counter[i] = ord(line[i - 1]) - 33
                else:
                    occurrence_counter[i] += 1
                    total_phred_counter[i] += ord(line[i - 1]) - 33

        # exit signal
        if line is None:
            out_list.put([occurrence_counter, total_phred_counter])
            return


def process_starter(shared_job_q, shared_result_q, nprocs):
    """ Split the work with jobs in shared_job_q and results in
        shared_result_q into several processes. Launch each process with
        counter as the worker function, and wait until all are
        finished.
    """
    procs = []
    for i in range(nprocs):
        p = Process(
                target=counter,
                args=(shared_job_q, shared_result_q))
        procs.append(p)
        p.start()

    for p in procs:
        p.join()


def make_client_manager(ip, port, authkey):
    """ Create a manager for a client. This manager connects to a server on the
        given address and exposes the get_job_q and get_result_q methods for
        accessing the shared queues from the server.
        Return a manager object.
    """
    class ServerQueueManager(SyncManager):
        pass

    ServerQueueManager.register('get_job_q')
    ServerQueueManager.register('get_result_q')

    manager = ServerQueueManager(address=(ip, port), authkey=authkey)
    manager.connect()

    print('Client connected to %s:%s' % (ip, port))
    return manager


def main():
    parser = argparse.ArgumentParser(prog='script.py', description=__doc__, formatter_class=argparse.HelpFormatter)
    parser.add_argument('-ip', help='ip of the manager server', required=True, nargs=1, type=str)
    parser.add_argument('-proc', help='number of processes on each node', required=False, default=[4], nargs=1,
                        type=int)
    parser.add_argument('-port', help='Port number on which to connect', required=True, nargs=1,
                        type=int)
    parser.add_argument('-key', help='authentication key', required=True, nargs=1,
                        type=str)
    args = parser.parse_args()
    numb_procs = args.proc[0]
    ip_host = args.ip[0]
    port = args.port[0]
    auth_key = args.key[0]

    manager = make_client_manager(ip_host, int(port), auth_key.encode(encoding='UTF-8'))
    job_q = manager.get_job_q()
    result_q = manager.get_result_q()
    process_starter(job_q, result_q, numb_procs)


if __name__ == "__main__":
    main()
