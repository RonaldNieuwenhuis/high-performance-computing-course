#!/bin/sh
#SBATCH --time=05:00:00
#SBATCH --nodes=1
#SBATCH --cpus-per-task=4

#SBATCH --mail-type=ALL
#SBATCH --mail-user=r.nieuwenhuis@st.hanze.nl
#SBATCH --output=/home/p225083/ronald/PerrysOutput4.txt

module load Python/3.5.1-intel-2016a 
python3 /home/p225083/ronald/script.py /home/p225083/rnaseq.fq -n 4
