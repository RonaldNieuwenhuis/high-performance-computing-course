import argparse
import csv
import datetime
import sys
import math
import functools
import operator
import itertools
from multiprocessing import Pool, Manager, Process
########################################################
# Author: Ronald Nieuwenhuis
# Assignment Big Data: Feel the pain!
# Due date: 18-05-'016
#######################################################

def counter(in_queue, out_list):
    """Count both the occurences of a base at a certain index and sum up their scores. """
    occurenceCounter = {}
    totalPhredCounter = {}
    while True:
        item = in_queue.get()
        line_no, line = item
        if line != None:
            for i in range(1, len(line)):
                if not i in totalPhredCounter and not i in occurenceCounter:
                    occurenceCounter[i] = 1
                    totalPhredCounter[i] = ord(line[i-1])-33
                else:
                    occurenceCounter[i] += 1
                    totalPhredCounter[i] += ord(line[i-1])-33
        
        # exit signal 
        if line == None:
            out_list.append([occurenceCounter, totalPhredCounter])
            return

def calcMean(occurenceCounter, totalPhredCounter):
    """Calculate the average score per index."""
    averageScores = {}
    for key, value in occurenceCounter.items():
        averageScores[key] = round(totalPhredCounter[key]/value)
    return averageScores

def mergeDicts(dictList):
    """Merge the dictionaries with the averagescores."""
    allKeys = functools.reduce(operator.or_, (set(d.keys()) for d in dictList), set())
    tempDict = {k: [d[k] for d in dictList if k in d] for k in allKeys}
    endResult = {key: round(sum(value)/len(value)) for key, value in tempDict.items()}
    return endResult

def main():
    begin = datetime.datetime.now()
    parser = argparse.ArgumentParser(prog='script.py', description=__doc__, formatter_class=argparse.HelpFormatter)
    parser.add_argument('infile', type=str, nargs=1)
    parser.add_argument('-n', help='number of processes spawned', required=False, default=[1], nargs=1, type=int)
    parser.add_argument('csvOut', nargs='?', type=str)
    args = parser.parse_args()

    numbProc = args.n[0]
    
    manager = Manager()
    results = manager.list()

    work = manager.Queue(numbProc)

    # start for workers    
    pool = []
    for i in range(numbProc):
        p = Process(target=counter, args=(work, results))
        p.start()
        pool.append(p)
    
    # produce data
    with open(args.infile[0]) as fastq:
        iters = itertools.chain(fastq, (None,)*numbProc*4)
        for num_and_line in enumerate(iters, start = 1):
            if num_and_line[0] % 4 == 0:
                work.put(num_and_line)
    
    for p in pool:
        p.join()
    
    timestamp_1 = datetime.datetime.now()
    resultsList = []

    print(results)
    print(results[0])
    for occurenceCounter, totalPhredCounter in results:
        averageScores = calcMean(occurenceCounter, totalPhredCounter)
        resultsList.append(averageScores)
    endResult = mergeDicts(resultsList)
    
    # Write output to csv file.
    timestamp_2 = datetime.datetime.now()
    if args.csvOut:
        with open(args.csvOut, mode="w") as csvFile:
            writer = csv.writer(csvFile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            for key, value in sorted(endResult.items()):
                writer.writerow([str(key), str(value)])
    else:
        for key, value in sorted(endResult.items()):
            print(key, value)

    end = datetime.datetime.now()
    print("Number of processes: ", numbProc)
    print("Total execution time: ", end - begin)
    print("Time used for iteration of fastq: ", timestamp_1 - begin)
    print("Time used for calculation of means and merging of dicts: ", timestamp_2 - timestamp_1)
    print("Time used for writing CSV: ", end - timestamp_2)
    print(numbProc, end - begin, timestamp_1 - begin, timestamp_2 - timestamp_1, end - timestamp_2)

if __name__ == "__main__":
    main()
